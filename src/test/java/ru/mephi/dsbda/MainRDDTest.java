package ru.mephi.dsbda;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.Optional;
import org.joda.time.DateTime;
import org.junit.Test;
import ru.mephi.dsbda.helpers.Helper;
import ru.mephi.dsbda.models.Salary;
import ru.mephi.dsbda.models.Statistic;
import ru.mephi.dsbda.models.Trip;
import ru.mephi.dsbda.spark.MainRDD;
import scala.Tuple2;

import java.util.UUID;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;


public class MainRDDTest {

    private MainRDD mainRDD = new MainRDD();

    /**
     * Test create new salary
     */
    @Test
    public void testNewSalary() {
        mainRDD.stop();

        int id = 1;
        String passport = "123456";
        DateTime birthday = DateTime.now();
        String month = "June";
        String salaries = "123123";

        Salary salary = new Salary(id, passport, birthday, month, salaries);

        assertNotNull(salary);
        assertEquals(1, salary.getId());
    }

    /**
     * Test create new static
     */
    @Test
    public void testNewStatic() {
        mainRDD.stop();

        UUID uuid = UUID.randomUUID();
        String ageCategory = "adult";
        double averageSalary = 123.3;
        double averageTrips = 123.3;
        Statistic statistic = new Statistic(uuid, ageCategory, averageSalary, averageTrips);

        assertNotNull(statistic);
        assertEquals("adult", statistic.getAgeCategory());
    }

    /**
     * Test new trip
     */
    @Test
    public void testNewTrip() {
        mainRDD.stop();

        int id = 1;
        String passport = "123456";
        DateTime birthday = DateTime.now();
        String month = "June";
        int trips = 3;

        Trip trip = new Trip(id, passport, birthday, month, trips);

        assertNotNull(trip);
        assertEquals(1, trip.getId());
    }

    /**
     * Test get age category
     */
    @Test
    public void testHelperAgeCategory() {
        mainRDD.stop();

        int year = 2015;

        assertEquals("children", Helper.ageCategory(year));
    }

    /**
     * Test get all salaries from Cassandra to RDD
     */
    @Test
    public void testGetSalaries() {
        JavaPairRDD<String, Salary> salaries = mainRDD.getSalaries();

        assertTrue(salaries.values().count() > 0);

        mainRDD.stop();
    }

    /**
     * Test get all trips from Cassandra to RDD
     */
    @Test
    public void testGetTrips() {
        JavaPairRDD<String, Trip> trips = mainRDD.getTrips();

        assertTrue(trips.values().count() > 0);

        mainRDD.stop();
    }

    /**
     * Test get all trips join with salaries, and summary Statistic
     */
    @Test
    public void testStatisticsRDD() {
        JavaPairRDD<String, Trip> averageTrips = mainRDD.getTrips();

        JavaPairRDD<String, Salary> averageSalaries = mainRDD.getSalaries();

        JavaPairRDD<String, Tuple2<Trip, Optional<Salary>>> joinedRDD = averageTrips.leftOuterJoin(averageSalaries);

        JavaPairRDD<String, Statistic> statisticsRDD = mainRDD.toStatistics(joinedRDD);

        JavaRDD<Statistic> resultsRDD = mainRDD.getResults(statisticsRDD);

        assertFalse(resultsRDD.isEmpty());

        mainRDD.stop();
    }





}
