package ru.mephi.dsbda.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import ru.mephi.dsbda.configs.CassandraConfig;
import ru.mephi.dsbda.helpers.Helper;
import ru.mephi.dsbda.models.Salary;
import ru.mephi.dsbda.models.Statistic;
import ru.mephi.dsbda.models.Trip;
import scala.Serializable;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import static com.datastax.spark.connector.japi.CassandraJavaUtil.javaFunctions;
import static com.datastax.spark.connector.japi.CassandraJavaUtil.mapRowTo;
import static com.datastax.spark.connector.japi.CassandraJavaUtil.mapToRow;

public class MainRDD implements Serializable {

    private transient JavaSparkContext sc;

    /**
     * Init MainRDD Process
     */
    public MainRDD() {
        CassandraConfig cassandraConfig = new CassandraConfig();

        SparkConf conf = new SparkConf();

        conf.setAppName("Homework #2");
        conf.setMaster("local[*]");
        conf.set("spark.cassandra.connection.host", cassandraConfig.getHost());

        sc = new JavaSparkContext(conf);
    }

    /**
     * Get mapped result from RDD
     * @param statisticsRDD
     * @return
     */
    public JavaRDD<Statistic> getResults(JavaPairRDD<String, Statistic> statisticsRDD) {
        return statisticsRDD.reduceByKey(new Function2<Statistic, Statistic, Statistic>() {
            @Override
            public Statistic call(Statistic v1, Statistic v2) throws Exception {
                v1.setAverageSalary((v1.getAverageSalary() + v2.getAverageSalary()) / 2);
                v1.setAverageTrips((v1.getAverageTrips() + v2.getAverageTrips()) / 2);
                return v1;
            }
        }).map(new Function<Tuple2<String, Statistic>, Statistic>() {
            @Override
            public Statistic call(Tuple2<String, Statistic> input) throws Exception {
                return new Statistic(UUID.randomUUID(), input._2().getAgeCategory(), input._2().getAverageSalary(), input._2().getAverageTrips());
            }
        });
    }

    /**
     * Joined data to Statistic
     * @param joinedRDD
     * @return
     */
    public JavaPairRDD<String, Statistic> toStatistics(JavaPairRDD<String, Tuple2<Trip, Optional<Salary>>> joinedRDD) {

        return joinedRDD.flatMapToPair(new PairFlatMapFunction<Tuple2<String, Tuple2<Trip, Optional<Salary>>>, String, Statistic>() {
            @Override
            public Iterator<Tuple2<String, Statistic>> call(Tuple2<String, Tuple2<Trip, Optional<Salary>>> input) throws Exception {
                Tuple2<Trip, Optional<Salary>> salaryWithTrip = input._2();

                List<Tuple2<String, Statistic>> statistic = new ArrayList<>();

                double salary = 0.0;

                if (salaryWithTrip._2().orNull() != null) {
                    salary = Double.parseDouble(salaryWithTrip._2().get().getSalary());
                }

                statistic.add(new Tuple2<>(Helper.ageCategory(salaryWithTrip._1().getBirthday().getYear()),
                                new Statistic(
                                        Helper.ageCategory(salaryWithTrip._1().getBirthday().getYear()),
                                        salary,
                                        salaryWithTrip._1().getTrips()
                                )
                        )
                );

                return statistic.iterator();
            }
        });
    }

    /**
     * Save statistics to Cassandra
     * @param statisticsRDD
     */
    public void save(JavaRDD<Statistic> statisticsRDD) {
        javaFunctions(statisticsRDD).writerBuilder("citizens", "statistics", mapToRow(Statistic.class)).saveToCassandra();
    }

    /**
     * Push all trips to RDD from Cassandra
     * @return
     */
    public JavaPairRDD<String, Trip> getTrips() {
        return javaFunctions(sc)
                .cassandraTable("citizens", "trips", mapRowTo(Trip.class))
                .keyBy(new Function<Trip, String>() {
                    @Override
                    public String call(Trip trip) throws Exception {
                        return trip.getPassport();
                    }
                });
    }

    /**
     * Push all salaries to RDD from Cassandra
     * @return
     */
    public JavaPairRDD<String, Salary> getSalaries() {
        return javaFunctions(sc)
                .cassandraTable("citizens", "salaries", mapRowTo(Salary.class))
                .keyBy(new Function<Salary, String>() {
                    @Override
                    public String call(Salary salary) throws Exception {
                        return salary.getPassport();
                    }
                });
    }


    /**
     * Stop SparkContext
     */
    public void stop() {
        sc.stop();
    }
}
