package ru.mephi.dsbda;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;

import ru.mephi.dsbda.models.Salary;
import ru.mephi.dsbda.models.Statistic;
import ru.mephi.dsbda.models.Trip;
import ru.mephi.dsbda.spark.MainRDD;
import scala.Tuple2;

import java.io.Serializable;



public class Main implements Serializable {

    /**
     * All homework tasks
     */
    private static void run() {
        MainRDD mainRDD = new MainRDD();

        JavaPairRDD<String, Trip> trips = mainRDD.getTrips();

        JavaPairRDD<String, Salary> salaries = mainRDD.getSalaries();

        JavaPairRDD<String, Tuple2<Trip, Optional<Salary>>> joinedRDD = trips.leftOuterJoin(salaries);

        JavaPairRDD<String, Statistic> statisticsRDD = mainRDD.toStatistics(joinedRDD);

        JavaRDD<Statistic> resultsRDD = mainRDD.getResults(statisticsRDD);

        mainRDD.save(resultsRDD);

        mainRDD.stop();
    }

    /**
     * Run main process
     * @param args
     */
    public static void main(String[] args) {
        run();
        System.exit(0);
    }
}

