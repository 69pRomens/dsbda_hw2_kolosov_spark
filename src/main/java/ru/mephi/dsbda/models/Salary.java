package ru.mephi.dsbda.models;

import org.joda.time.DateTime;

import java.io.Serializable;

public class Salary implements Serializable {
    private int id;
    private String passport;
    private DateTime birthday;
    private String month;
    private String salary;

    public Salary (int id, String passport, DateTime birthday, String month, String salary) {
        this.id = id;
        this.passport = passport;
        this.birthday = birthday;
        this.month = month;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public DateTime getBirthday() {
        return birthday;
    }

    public void setBirthday(DateTime birthday) {
        this.birthday = birthday;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }


}
