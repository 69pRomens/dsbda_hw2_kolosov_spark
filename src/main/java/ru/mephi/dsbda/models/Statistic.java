package ru.mephi.dsbda.models;

import java.io.Serializable;
import java.util.UUID;

public class Statistic implements Serializable {
    private UUID uuid;
    private String ageCategory;
    private double averageSalary;
    private double averageTrips;

    public Statistic(String ageCategory) {
        this.ageCategory = ageCategory;
    }

    public Statistic(UUID uuid, String ageCategory, double averageSalary, double averageTrips)
    {
        this.uuid = uuid;
        this.ageCategory = ageCategory;
        this.averageSalary = averageSalary;
        this.averageTrips = averageTrips;
    }


    public Statistic(String ageCategory, double averageSalary, double averageTrips)
    {
        this.ageCategory = ageCategory;
        this.averageSalary = averageSalary;
        this.averageTrips = averageTrips;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getAgeCategory() {
        return ageCategory;
    }

    public void setAgeCategory(String ageCategory) {
        this.ageCategory = ageCategory;
    }

    public double getAverageSalary() {
        return averageSalary;
    }

    public void setAverageSalary(double averageSalary) {
        this.averageSalary = averageSalary;
    }

    public double getAverageTrips() {
        return averageTrips;
    }

    public void setAverageTrips(double averageTrips) {
        this.averageTrips = averageTrips;
    }

    @Override
    public String toString() {
        return "Statistic{" +
                "uuid=" + uuid +
                ", ageCategory='" + ageCategory + '\'' +
                ", averageSalary=" + averageSalary +
                ", averageTrips=" + averageTrips +
                '}';
    }
}
