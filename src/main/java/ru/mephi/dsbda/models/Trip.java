package ru.mephi.dsbda.models;

import com.datastax.spark.connector.types.TimestampType;
import org.apache.spark.sql.catalyst.CatalystTypeConverters;
import org.joda.time.DateTime;

import java.io.Serializable;
import java.text.SimpleDateFormat;


public class Trip implements Serializable {


    private int id;
    private String passport;
    private DateTime birthday;
    private String month;
    private int trips;

    public Trip (int id, String passport, DateTime birthday, String month, int trips) {
        this.id = id;
        this.passport = passport;
        this.birthday = birthday;
        this.month = month;
        this.trips = trips;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public DateTime getBirthday() {
        return birthday;
    }

    public void setBirthday(DateTime birthday) {
        this.birthday = birthday;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getTrips() {
        return trips;
    }

    public void setTrips(int trips) {
        this.trips = trips;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "id=" + id +
                ", passport='" + passport + '\'' +
                ", birthday=" + birthday +
                ", month='" + month + '\'' +
                ", trips=" + trips +
                '}';
    }
}
