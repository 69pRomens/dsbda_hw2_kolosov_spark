package ru.mephi.dsbda.helpers;

import java.time.LocalDate;

public class Helper {
    /**
     * Get category from citizen age
     * @param year
     * @return
     */
    public static String ageCategory(Integer year) {
        try {

            int age = LocalDate.now().getYear() - year;

            String category;

            if (age < 17) {
                category="children";
            } else if (age < 30) {
                category="young-people";
            } else if (age < 60) {
                category="adults";
            } else if (age <= 80){
                category="old-aged";
            } else if (age < 2018){
                category="long-livers";
            } else {
                category="unknown";
            }
            return category;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Unknown";
    }
}
